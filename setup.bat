cmake -S . -B ./build/windows/msvc/64-bit/Debug -DCMAKE_BUILD_TYPE=Debug
cmake --build ./build/windows/msvc/64-bit/Debug --config Debug

cmake -S . -B ./build/windows/msvc/64-bit/Release -DCMAKE_BUILD_TYPE=Release
cmake --build ./build/windows/msvc/64-bit/Release --config Release