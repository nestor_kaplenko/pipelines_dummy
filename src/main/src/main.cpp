#include <iostream>

#include "entity.h"

int main()
{
    Entity entity;
    std::cout << entity.getGreeting() << std::endl;

    std::cin.get();
    return 0;
}
