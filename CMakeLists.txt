cmake_minimum_required( VERSION 3.14 )
#enable_testing(true)
project(Bitbucket_Pipeline_Dummy LANGUAGES CXX)

set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_AUTOUIC ON )
set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTORCC ON )

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_CXX_STANDARD_REQUIRED ON )

set( TARGET_NAME PipelinesDummy)

set( TEST_TARGET_NAME ${TARGET_NAME}_Tests)

if (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    set( Qt5_DIR ${CMAKE_SOURCE_DIR}/thirdparty/qt/msvc2017_64)
    set( COMPILER_NAME_STRING msvc)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    set( Qt5_DIR ${CMAKE_SOURCE_DIR}/thirdparty/qt/mingw73_64)
    set( COMPILER_NAME_STRING mingw)
else()
    message( FATAL_ERROR "Compiler is not recognized" )
endif()



if (CMAKE_BUILD_TYPE STREQUAL "Debug")
file(GLOB QT_COMPONENTS_DLLS
  ${Qt5_DIR}/bin/Qt5Cored.dll
  ${Qt5_DIR}/bin/Qt5Guid.dll
  ${Qt5_DIR}/bin/Qt5Widgetsd.dll
  ${Qt5_DIR}/bin/Qt5Testd.dll
)
endif()

if (CMAKE_BUILD_TYPE STREQUAL "Release")
file(GLOB QT_COMPONENTS_DLLS
  ${Qt5_DIR}/bin/Qt5Core.dll
  ${Qt5_DIR}/bin/Qt5Gui.dll
  ${Qt5_DIR}/bin/Qt5Widgets.dll
  ${Qt5_DIR}/bin/Qt5Test.dll
)
endif()

add_subdirectory( src/main )
add_subdirectory( test/main )

