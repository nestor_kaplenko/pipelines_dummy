#include "entity_tests.h"


void TestEntity::testHelloMethod()
{
    Entity entity;
    QVERIFY( entity.getGreeting() == "Greetings from the Entity class!" );
}
