#include <QtTest/QTest>
#include <iostream>
#include "entity_tests.h"


int main( int argc, char* argv[] )
{
    // QTest::qExec(new TestArc,  argc, argv);
    int  status  = 0;
    auto runTest = [&status, argc, argv]( QObject* obj )
    {
        status |= QTest::qExec( obj, argc, argv );
    };

    // run suite
    runTest( new TestEntity );

    std::cin.get();
    return status;
}
